<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

if(have_posts()){
    while(have_posts()){
        the_post();
        ?>
        <div id="card">
        <h1><?php echo the_field('titre'); ?></h1>
        <img src="<?php the_field('image'); ?>" id="img">
        <p id="description">Description : <?php  the_field('description'); ?></p>
        <p id="date">Date de création : <?php the_field('date_de_creation'); ?></p>
        <?php if(get_field('nombre_de_version')):?>
        <p id="version">Nombre de version : <?php  the_field('nombre_de_version'); ?></p>
        <?php endif; ?>
        <p id="projet">Projet en cours : <?php  the_field('projet_en_cours'); ?></p>
        <p id="framework">Framework utilisé ? <?php  the_field('checkbox'); ?></p>
            <?php if(get_field('complements')):?>
        <p id="complement"> Compléments : <?php the_field('complements'); ?></p>
        <?php endif; ?>
        </div>
<?php
      the_content();
    }
}
rewind_posts();
get_footer();
?>