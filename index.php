<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

if(have_posts()){
    while(have_posts()){
        the_content();
        the_title();
        the_post();
    }
}
get_footer();
rewind_posts();

