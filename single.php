<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

if(have_posts()){
    while(have_posts()){
        the_title();
        the_post();
        echo get_the_content();
    }
}
get_footer();
rewind_posts();