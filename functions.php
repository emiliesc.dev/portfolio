<?php

add_action('wp_enqueue_scripts', 'enqueue_portfolio_style');
function enqueue_portfolio_style(){
    wp_enqueue_style('portfolio',
    get_stylesheet_directory_uri().'/style.css',
    array('hello-elementor'),
    wp_get_theme()->get('Version')
    );
}
add_action('init', 'create_member_cpt');
function create_member_cpt(){
    $args = array(
        'labels' => array(
        'name'                  => _x( 'Projet', 'Post type general name', 'portfolio' ),
        'singular_name'         => _x( 'Projet', 'Post type singular name', 'portfolio' ),
        'menu_name'             => _x( 'Projets', 'Admin Menu text', 'portfolio' ),
        'name_admin_bar'        => _x( 'Projets', 'Add New on Toolbar', 'portfolio' ),
        'add_new'               => __( 'Ajouter un nouveau projet', 'portfolio' ),
        'add_new_item'          => __( 'Ajouter un nouveau projet', 'portfolio' ),
        'new_item'              => __( 'Nouveau projet', 'portfolio' ),
        'edit_item'             => __( 'Editer un projet', 'portfolio' ),
        'view_item'             => __( 'Voir un projet', 'portfolio' ),
        'all_items'             => __( 'Tous les projets', 'portfolio' ),
        'search_items'          => __( 'Chercher un projet', 'portfolio' ),
        'parent_item_colon'     => __( 'item parent', 'portfolio' ),
        'not_found'             => __( 'Pas de projet trouvé', 'portfolio' ),
        'not_found_in_trash'    => __( 'Pas de projet trouvé dans la corbeille', 'portfolio' ),
        'featured_image'        => _x( 'Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'portfolio' ),
        'set_featured_image'    => _x( 'Appliquer image' , 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'portfolio' ),
        'remove_featured_image' => _x( 'Supprimer image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'portfolio' ),
        'use_featured_image'    => _x( 'Utiliser image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'portfolio' ),
        'archives'              => _x( 'Projet', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'portfolio' ),
        'insert_into_item'      => _x( 'Ajouter au projet', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'portfolio' ),
        'uploaded_to_this_item' => _x( 'Telecharger', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'portfolio' ),
        'filter_items_list'     => _x( 'Filtrer', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'portfolio' ),
        'items_list_navigation' => _x( 'Navigation des articles', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'portfolio' ),
        'items_list'            => _x( 'Liste des articles', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'portfolio' ),
    ),
        'public' => true, //permet de voir le posttype dans l'interface admin et front
        'hierarchical' => false, //fait que le posttype se comporte comme un article et pas une page
        'exclude_from_search' => false, //le post type apparait pas dans les resultats de recherche
        'publicly_queryable' => true, //
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menu' => false,
        'show_in_admin_bar' => true,
        'show_in_rest' => true, //requete AJAX a voir en JS
        'menu_position' => 6, //organise dashboard
        'menu_icon' => 'dashicons-album', //picto interface admin
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ), //element constituant le post
        'taxonomies' => array('category'),
        'has_archive' => true,
        );
    register_post_type('portfolio', $args);
}

add_action('init', 'create_personnal_cpt');
function create_personnal_cpt(){
    $args1 = array(
        'labels' => array(
            'name'                  => _x( 'Projet personnel', 'Post type general name', 'portfolio' ),
            'singular_name'         => _x( 'Projet personnel', 'Post type singular name', 'portfolio' ),
            'menu_name'             => _x( 'Projets perso', 'Admin Menu text', 'portfolio' ),
            'name_admin_bar'        => _x( 'Projets perso', 'Add New on Toolbar', 'portfolio' ),
            'add_new'               => __( 'Ajouter un nouveau projet personnel', 'portfolio' ),
            'add_new_item'          => __( 'Ajouter un nouveau projet personnel', 'portfolio' ),
            'new_item'              => __( 'Nouveau projet personnel', 'portfolio' ),
            'edit_item'             => __( 'Editer un projet personnel', 'portfolio' ),
            'view_item'             => __( 'Voir un projet personnel', 'portfolio' ),
            'all_items'             => __( 'Tous les projets personnels', 'portfolio' ),
            'search_items'          => __( 'Chercher un projet personnel', 'portfolio' ),
            'parent_item_colon'     => __( 'item parent', 'portfolio' ),
            'not_found'             => __( 'Pas de projet personnel trouvé', 'portfolio' ),
            'not_found_in_trash'    => __( 'Pas de projet personnel trouvé dans la corbeille', 'portfolio' ),
            'featured_image'        => _x( 'Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'portfolio' ),
            'set_featured_image'    => _x( 'Appliquer image' , 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'portfolio' ),
            'remove_featured_image' => _x( 'Supprimer image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'portfolio' ),
            'use_featured_image'    => _x( 'Utiliser image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'portfolio' ),
            'archives'              => _x( 'Projet perso', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'portfolio' ),
            'insert_into_item'      => _x( 'Ajouter au projet', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'portfolio' ),
            'uploaded_to_this_item' => _x( 'Telecharger', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'portfolio' ),
            'filter_items_list'     => _x( 'Filtrer', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'portfolio' ),
            'items_list_navigation' => _x( 'Navigation des articles', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'portfolio' ),
            'items_list'            => _x( 'Liste des articles', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'portfolio' ),
        ),
        'public' => true, //permet de voir le posttype dans l'interface admin et front
        'hierarchical' => false, //fait que le posttype se comporte comme un article et pas une page
        'exclude_from_search' => false, //le post type apparait pas dans les resultats de recherche
        'publicly_queryable' => true, //
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menu' => false,
        'show_in_admin_bar' => true,
        'show_in_rest' => true, //requete AJAX a voir en JS
        'menu_position' => 7, //organise dashboard
        'menu_icon' => 'dashicons-shortcode', //picto interface admin
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ), //element constituant le post
        'taxonomies' => array('category'),
        'has_archive' => true,
    );
    register_post_type('personnal', $args1);
}

